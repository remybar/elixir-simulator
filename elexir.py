#!/usr/bin/python
import argparse
import math
from pathlib import Path
from datetime import datetime

# ----- Data from Elexir app ----- 
BASE_DAILY_ROI = 0.2
CLAIMSTONE_COST = 10
COOKBOOK_COST = 2
MAX_MANA = 100
CAULDRON_BUY_MANA =	12
COOKBOOK_BUY_MANA =	1
CLAIMSTONE_BUY_MANA = 1
CAULDRON_LEVEL_UP_MANA = 20
CAULDRON_CLAIM_MANA = 60
MANA_HOUR_RATE_RELOAD =	1.5

INCREASE_TIME_COMMAND = '1'
CLAIM_REWARDS_COMMAND = '2'
SHOW_STATUS_COMMAND = '3'
LEVEL_UP_COMMAND = '4'
LEVEL_UP_MAX_COMMAND = '5'
BUY_CLAIMSTONES_COMMAND = '6'
BUY_COOKBOOKS_COMMAND = '7'
BUY_CAULDRONS_COMMAND = '8'
BUY_ELEXIR_COMMAND = '9'

REPORT_FOLDER = Path('reports')

def error(msg):
    print(f"[ERROR] {msg} !!!")

class Cauldron:
    """
    Manage a cauldron
    """
    NEXT_CAULDRON_NAME = 'A'

    def __init__(self, level=1, last_claim_time_in_hours=0):
        self.last_claim_time_in_hours = last_claim_time_in_hours
        self.name = Cauldron.NEXT_CAULDRON_NAME.upper()
        self.level = level

        Cauldron.NEXT_CAULDRON_NAME = chr(ord(Cauldron.NEXT_CAULDRON_NAME) + 1)

    def get_nb_stones_for_claiming(self):
        """ get the number of required claimstones to claim reward from the cauldron """
        return int(math.sqrt(self.level))

    def compute_rewards(self, today_in_hours):
        """ compute the actual number of $EXLR produced by the cauldron """
        return (today_in_hours - self.last_claim_time_in_hours) / 24.0 * self.level * BASE_DAILY_ROI

    def claim_rewards(self, today_in_hours):
        """ get rewards from the cauldron and so, reset the last claim time """
        rewards = self.compute_rewards(today_in_hours)
        self.last_claim_time_in_hours = today_in_hours
        return rewards

    def level_up(self, level_count=1):
        """ increase the cauldron's level """
        self.level += level_count

    def level_up_cost_in_elxr(self, level_count=1):
        """ compute the cost in $ELXR to level-up a cauldron of `level_count` levels """
        return self.level_up_cost_in_books(level_count) * COOKBOOK_COST

    def level_up_cost_in_books(self, level_count=1):
        """ compute the cost in cookbooks to level-up a cauldron of `level_count` levels """
        return sum((self.level + i) for i in range(level_count))

    def get_status(self, today_in_hours):
        """ show the actual status of the cauldron """
        return (
            "    Cauldron {}  level: {}   ELXR: {:.2f}   last claim time: {:.2f} hours ({:.2f} days)".format(
                self.name,
                self.level,
                self.compute_rewards(today_in_hours),
                self.last_claim_time_in_hours,
                self.last_claim_time_in_hours / 24.0,
            )
        )

class Elexir:
    """
    Manage the Elexir cauldron farm
    """
    def __init__(self, init_elxr, init_claimstones, init_cookbooks, cauldrons):
        self.consumed_cookbooks = 0
        self.consumed_claimstones = 0
        self.current_time_in_hours = 0
        self.current_elxr = init_elxr
        self.current_mana = MAX_MANA
        self.available_claimstones = init_claimstones
        self.available_cookbooks = init_cookbooks
        self.cauldrons = cauldrons

        folder = datetime.now().strftime("%Y%m%d_%H-%M-%S")
        (REPORT_FOLDER / folder).mkdir()
        self.report_file = open(REPORT_FOLDER / folder / "report.txt", 'w')
        self.record_file = open(REPORT_FOLDER / folder / "commands.txt", 'w')

    def __del__(self):
        self.report_file.close()

    def increase_time(self, hours_count):
        """
        Move to the future of `hours_count` hours, and update available mana.
        """
        self.current_time_in_hours += hours_count
        self.current_mana += hours_count * MANA_HOUR_RATE_RELOAD
        self.current_mana = min(self.current_mana, MAX_MANA)

    def adjust_time_from_available_mana(self, needed_mana=0):
        """
        If some operations cost too many mana, just increase the time accordingly to simulate
        that we wait long enough to be able to do these operations.
        Or just increase the time enough to get `needed_mana`
        """
        if self.current_mana < 0:
            value = math.ceil(-1 * self.current_mana / MANA_HOUR_RATE_RELOAD)
            self.report_file.write("TOO MANY MANA CONSUMED! => increase time of {:.2f} hours\n".format(value))
        elif needed_mana:
            value = math.ceil(needed_mana / MANA_HOUR_RATE_RELOAD)
            self.report_file.write("{} more mana needed => increase time of {:.2f} hours\n".format(needed_mana, value))
        else:
            value = 0

        if value:            
            self.increase_time(value)

    def select_cauldron(self, cauldron_name):
        """
        Select a cauldron with its name
        """
        found = [c for c in self.cauldrons if c.name == cauldron_name]
        if len(found) != 1:
            error("Unable to find the cauldron named '%s'" % cauldron_name)
        return found[0]

    def claim_rewards(self, cauldron_name):
        """
        Claim rewards from a cauldron, update available mana.
        """
        cauldron = self.select_cauldron(cauldron_name)
        claiming_cost_in_stones = cauldron.get_nb_stones_for_claiming()

        if claiming_cost_in_stones <= self.available_claimstones:
            if self.current_mana < CAULDRON_CLAIM_MANA:
                self.adjust_time_from_available_mana(needed_mana=CAULDRON_CLAIM_MANA)

            self.current_elxr += cauldron.claim_rewards(self.current_time_in_hours)
            self.current_mana -= CAULDRON_CLAIM_MANA
            self.available_claimstones -= claiming_cost_in_stones
            self.consumed_claimstones += claiming_cost_in_stones
        else:
            error("Not enough claimstones, your cauldron has exploded")

    def buy_cookbooks(self, count):
        """
        Buy some cookbooks and update available mana.
        """
        cost = count * COOKBOOK_COST
        if cost <= self.current_elxr:
            self.current_elxr -= cost
            self.current_mana -= count * COOKBOOK_BUY_MANA
            self.available_cookbooks += count
        else:
            error(f"Not enough $ELXR to buy {count} cookbooks")

    def buy_claimstones(self, count):
        """
        Buy some claimstones and update available mana.
        """
        cost = count * CLAIMSTONE_COST

        if cost <= self.current_elxr:
            self.current_elxr -= count * CLAIMSTONE_COST
            self.current_mana -= count * CLAIMSTONE_BUY_MANA
            self.available_claimstones += count
        else:
            error(f"Not enough $ELXR to buy {count} claimstones")

    def buy_cauldrons(self, count, cost):
        """
        Buy some cauldrons and update available mana.
        """
        total_cost = count * cost

        if total_cost <= self.current_elxr:
            self.current_elxr -= total_cost
            self.current_mana -= count * CAULDRON_BUY_MANA
            for i in range(count):
                self.cauldrons.append(Cauldron(last_claim_time_in_hours=self.current_time_in_hours))
        else:
            error(f"Not enough $ELXR to buy {count} cauldrons at {cost} $ELXR")

    def get_daily_production_rate(self):
        """
        Compute the daily production rate of the whole farm.
        """
        return sum(c.level for c in self.cauldrons) * BASE_DAILY_ROI

    def _level_cauldron_up_to_one_level(self, cauldron):
        """
        Internal method to level-up a cauldron of one level after all checks done
        """
        cost_in_books = cauldron.level_up_cost_in_books()

        cauldron.level_up()
        self.current_mana -= CAULDRON_LEVEL_UP_MANA
        self.available_cookbooks -= cost_in_books
        self.consumed_cookbooks += cost_in_books

        self.report_file.write("It cost you {} cookbooks to level up from {} to {}\n".format(
            cost_in_books, cauldron.level - 1, cauldron.level,
        ))

    def level_up(self, cauldron_name, level_count):
        """
        Level-up a cauldron of `level_count` level and update available mana.
        """
        cauldron = self.select_cauldron(cauldron_name)

        for level in range(level_count):
            cost_in_books = cauldron.level_up_cost_in_books()

            if cost_in_books <= self.available_cookbooks:
                self._level_cauldron_up_to_one_level(cauldron)
            else:
                error("Not enough cookbooks to level-up your cauldron from {} to {}".format(
                    cauldron.level, cauldron.level + 1,
                ))
                break

    def level_up_max(self, cauldron_name):
        """
        Level-up a cauldron to the maximum possible level and update available mana.
        """
        cauldron = self.select_cauldron(cauldron_name)
        cost_in_books = cauldron.level_up_cost_in_books()

        while (cost_in_books <= self.available_cookbooks):
            self._level_cauldron_up_to_one_level(cauldron)
            cost_in_books = cauldron.level_up_cost_in_books()

    def format_current_time(self):
        """
        Format the current time in days + hours.
        """
        days = int(self.current_time_in_hours / 24.0)
        hours = self.current_time_in_hours - days * 24
        if hours:
            return f"{days} (+ {hours} hours)"
        return f"{days}"

    def show_status(self):
        """
        Show the actual status of the farm.
        """
        print("----------------------------------")
        print("Current time in hours : %d" % self.current_time_in_hours)
        print("Current time in days  : %s" % self.format_current_time())
        print("Available mana        : {:.2f}".format(self.current_mana))
        print("Available $ELXR       : {:.2f}".format(self.current_elxr))
        print("Available claimstones : {}".format(self.available_claimstones))
        print("Available cookbooks   : {}".format(self.available_cookbooks))
        print("Consumed cookbooks    : %d" % self.consumed_cookbooks)
        print("Consumed claimstones  : %d" % self.consumed_claimstones)
        print("Daily production rate : {:.2f}".format(self.get_daily_production_rate()))
        print("Cauldrons             :")
        for c in self.cauldrons:
            print(c.get_status(self.current_time_in_hours))

    def log_status(self):
        """
        Log the actual status of the farm in the report file.
        """
        self.report_file.write("----------------------------------\n")
        self.report_file.write("Current time in hours : %d\n" % self.current_time_in_hours)
        self.report_file.write("Current time in days  : %s\n" % self.format_current_time())
        self.report_file.write("Available mana        : {:.2f}\n".format(self.current_mana))
        self.report_file.write("Available $ELXR       : {:.2f}\n".format(self.current_elxr))
        self.report_file.write("Available claimstones : {}\n".format(self.available_claimstones))
        self.report_file.write("Available cookbooks   : {}\n".format(self.available_cookbooks))
        self.report_file.write("Consumed cookbooks    : %d\n" % self.consumed_cookbooks)
        self.report_file.write("Consumed claimstones  : %d\n" % self.consumed_claimstones)
        self.report_file.write("Daily production rate : {:.2f}\n".format(self.get_daily_production_rate()))
        self.report_file.write("Cauldrons             :\n")
        for c in self.cauldrons:
            self.report_file.write("%s\n" % c.get_status(self.current_time_in_hours))
        self.report_file.write("----------------------------------\n")

    def load_scenario(self, filename):
        """
        Load a scenario from a file.
        Each line should be <COMMAND_NUMBER> <PARAMS separated by a space>
        """
        lines = open(filename, 'r').readlines()
        for l in lines:
            l = l[:l.find('#')] if '#' in l else l  # remove comments
            command, *params = l.split()
            self.process(command, params)

    def process_increase_time(self, params=None):
        """
        Process the 'increase time' command either from command-line or from file (params not None)
        """
        if params:
            value = params[0]
        else:
            print("How many time ? (in hours by default, add 'd' at the end for days")
            value = input()
        value = int(value[:-1]) * 24.0 if value.endswith('d') else int(value)

        for f in (self.report_file, self.record_file):
            f.write("{cmd} {hours}       # increase of {hours} ({days:.2f} days)\n".format(
                cmd=INCREASE_TIME_COMMAND,
                hours=int(value),
                days=value/24.0,
            ))
        self.increase_time(value)

    def process_claim_rewards(self, params=None):
        """
        Process the 'claim rewards' command either from command-line or from file (params not None)
        """
        if params:
            cauldron_name = params[0]
        else:
            print("Which cauldron ?")
            cauldron_name = input().upper()

        for f in (self.report_file, self.record_file):
            f.write("{cmd} {name}       # claim rewards for cauldron '{name}'\n".format(
                cmd=CLAIM_REWARDS_COMMAND,
                name=cauldron_name,
            ))
        self.claim_rewards(cauldron_name)

    def process_level_up(self, params=None):
        """
        Process the 'level-up' command either from command-line or from file (params not None)
        """
        if params:
            cauldron_name = params[0]
            level_count = int(params[1])
        else:
            print("Which cauldron ?")
            cauldron_name = input().upper()
            print("How many levels ?")
            level_count = int(input())

        for f in (self.report_file, self.record_file):
            f.write("{cmd} {name} {count}       # level-up cauldron '{name}' of {count} levels\n".format(
                cmd=LEVEL_UP_COMMAND,
                name=cauldron_name,
                count=level_count,
            ))
        self.level_up(cauldron_name, level_count)

    def process_level_up_max(self, params=None):
        """
        Process the 'level-up' max command either from command-line or from file (params not None)
        """
        if params:
            cauldron_name = params[0]
        else:
            print("Which cauldron ?")
            cauldron_name = input().upper()

        for f in (self.report_file, self.record_file):
            f.write("{cmd} {name}       # level-up cauldron '{name}' to level max\n".format(
                cmd=LEVEL_UP_MAX_COMMAND,
                name=cauldron_name,
            ))
        self.level_up_max(cauldron_name)

    def process_buy_claimstones(self, params=None):
        """
        Process the 'buy claimstones' max command either from command-line or from file (params not None)
        """
        if params:
            count = int(params[0])
        else:
            print("How many ?")
            count = int(input())

        for f in (self.report_file, self.record_file):
            f.write("{cmd} {count}       # by {count} claimstones\n".format(
                cmd=BUY_CLAIMSTONES_COMMAND,
                count=count,
            ))
        self.buy_claimstones(count)

    def process_buy_cookbooks(self, params=None):
        """
        Process the 'buy cookbooks' max command either from command-line or from file (params not None)
        """
        if params:
            count = int(params[0])
        else:
            print("How many ?")
            count = int(input())

        for f in (self.report_file, self.record_file):
            f.write("{cmd} {count}       # by {count} cookbooks\n".format(
                cmd=BUY_COOKBOOKS_COMMAND,
                count=count,
            ))
        self.buy_cookbooks(count)

    def process_buy_cauldrons(self, params=None):
        """
        Process the 'buy cauldrons' max command either from command-line or from file (params not None)
        """
        if params:
            count = int(params[0])
            cost = int(params[1])
        else:
            print("How many ?")
            count = int(input())
            print("What cost ?")
            cost = int(input())

        for f in (self.report_file, self.record_file):
            f.write("{cmd} {count} {cost}      # by {count} cauldrons at {cost} $ELXR\n".format(
                cmd=BUY_CAULDRONS_COMMAND,
                count=count,
                cost=cost,
            ))
        self.buy_cauldrons(count, cost)

    def process_buy_elexir(self, params=None):
        """
        Process the 'buy $ELXR' max command either from command-line or from file (params not None)
        """
        if params:
            count = int(params[0])
        else:
            print("How many ?")
            count = int(input())

        for f in (self.report_file, self.record_file):
            f.write("{cmd} {count}      # by {count} $ELXR\n".format(
                cmd=BUY_ELEXIR_COMMAND,
                count=count,
            ))
        self.current_elxr += count

    def process(self, choice, params=None):
        """
        Process commands.
        """
        if choice == INCREASE_TIME_COMMAND:
            self.process_increase_time(params)
        elif choice == CLAIM_REWARDS_COMMAND:
            self.process_claim_rewards(params)
        elif choice == SHOW_STATUS_COMMAND:
            self.show_status()
        elif choice == LEVEL_UP_COMMAND:
            self.process_level_up(params)
        elif choice == LEVEL_UP_MAX_COMMAND:
            self.process_level_up_max(params)
        elif choice == BUY_CLAIMSTONES_COMMAND:
            self.process_buy_claimstones(params)
        elif choice == BUY_COOKBOOKS_COMMAND:
            self.process_buy_cookbooks(params)
        elif choice == BUY_CAULDRONS_COMMAND:
            self.process_buy_cauldrons(params)
        elif choice == BUY_ELEXIR_COMMAND:
            self.process_buy_elexir(params)
        elif choice == 'l':
            filename = input("Which file?")
            self.load_scenario(filename)
        else:
            print("wrong command")

        self.adjust_time_from_available_mana()
        self.log_status()

    def show_menu(self):
        """
        Show the app menu
        """
        print("----------------------------------")
        print("{cmd}) increase time".format(cmd=INCREASE_TIME_COMMAND))
        print("{cmd}) get rewards from cauldron".format(cmd=CLAIM_REWARDS_COMMAND))
        print("{cmd}) show status".format(cmd=SHOW_STATUS_COMMAND))
        print("{cmd}) level-up a cauldron".format(cmd=LEVEL_UP_COMMAND))
        print("{cmd}) level-up MAX a cauldron".format(cmd=LEVEL_UP_MAX_COMMAND))
        print("{cmd}) buy claimstones".format(cmd=BUY_CLAIMSTONES_COMMAND))
        print("{cmd}) buy cookbooks".format(cmd=BUY_COOKBOOKS_COMMAND))
        print("{cmd}) buy cauldron".format(cmd=BUY_CAULDRONS_COMMAND))
        print("{cmd}) buy $ELXR".format(cmd=BUY_ELEXIR_COMMAND))
        print("l) load scenario")
        print("q) quit")
        print("----------------------------------")
        return input().lower()

    def run(self, args):
        """
        Run the farm!
        """
        if args.input:
            self.load_scenario(args.input)

        self.show_status()
        self.log_status()
        choice = self.show_menu()

        while choice != 'q':
            self.process(choice)
            self.show_status()
            choice = self.show_menu()


# Initial state of the farm
INIT_ELXR = 0
INIT_CLAIMSTONES = 0
INIT_COOKBOOKS = 0
INIT_CAULDRONS = []

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', type=str, dest='input')
    return parser.parse_args()

def main():
    args = parse_arguments()

    elexir = Elexir(
        init_elxr = INIT_ELXR,
        init_claimstones = INIT_CLAIMSTONES,
        init_cookbooks = INIT_COOKBOOKS,
        cauldrons = INIT_CAULDRONS,
    )
    elexir.run(args)
    print("Bye!")

if __name__ == "__main__":
    main()
