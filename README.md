# Elexir Simulator

This script is a rought simulator of Elexir rules. You can use it to simulate your strategy and see if everything fit (mana usage, $ELXR, number of claimstones...).
This script is **NOT strongly tested**, so some bugs might happen ... If you see some issues, don't hesitate to tell me or even better,
create a pull request with the fix, I will merge it ;-)

## Usage

You need to install Python3 to use this simulator.

A list of available commands is displayed when the script starts:

* **(1) increase time**: move the current date in the future. `10d` will move to 10 days in the future, `10` will move to 10 hours.  
* **(2) get rewards**: claim rewards from a cauldron (you need claimstone(s) for that)
* **(3) show status**: show the actual status of your cauldrons, your mana, your $ELXR, ...
* **(4) level-up a cauldron**: increase the level of a cauldron (you need cookbook(s) for that)
* **(5) level-up MAX a cauldron**: increase the level of a cauldron to its maximum possible value according to the number of cookbooks you have.
* **(6) buy claimstones**: just buy claimstones with available $ELXR
* **(7) buy cookbooks**: just buy cookbooks with available $ELXR
* **(8) buy cauldron**: just buy new cauldrons
* **(9) buy $ELXR**: just buy more $ELXR
* **(l) load a scenario**: load a scenario file (see next chapters)
* **(q) quit**: quit the simulator :-)

Note that if you choose an action and you haven't enough mana to do it, the action is done BUT the current date is automatically increased to simulate the fact you wait for reload your mana.

## Report files

Each time you use the script, a new folder is created in `report`. This new folder contains:
* a `report.txt` file which contains all the commands you used + the status of your cauldron farm after each command. It's helpful to understand what happen at each step.
* a `commands.txt` file which contains all the commands you used. You can directly reload this file using the command `(l) load a scenario` to replay it.

## Scenario format

In order to avoid entering multiple times the same commands at the beginning of your tests, you can create scenario files that you are able to load using the `(l) load a scenario`. Once loaded, you can continue to use the script, load another scenario, ...

The format is quite simple:
`<COMMAND> <PARAMETERS> # comments`

For example:
```
8 5 30      # by 5 cauldrons at 30 $ELXR
6 2         # by 2 claimstones
7 15        # by 15 cookbooks
5 A         # level-up cauldron 'A' to max
```

Enjoy your tests! :-)
